---
title: Jasa Pembuatan Website Berkualitas Semarang - Indonesia | Igumedia
description: Igumedia merupakan jasa pembuatan website dengan desain responsive dan
  ramah SEO. Igumedia fokus menyajikan produk dan nilai jual anda agar dapat mengkonversi
  pengunjung menjadi pelanggan.
hero:
  heading: Kamu butuh jasa pembuatan website?
  subheading: Kamia akan membuatkan kamu website yang cepat, stabil dan dapat mengkomunikasikan
    produkmu.
  cta: Buat websitemu sekarang!
intro:
  heading: Tentang Igumedia
  text: Igumedia berspesialisasi dalam membangun website informasional seperti brosur
    online, company profile, blog, portal majalah/berita, website institusi, sekolah,
    dll. Igumedia menempatkan customer Anda dan SEO sebagai prioritas utama. Igumedia
    memandang proyek pembuatan website dari perspektif customer anda untuk membantu
    Anda menyajikan informasi yang tepat untuk membangun bisnis Anda.
services:
  services1:
    heading: Berkualitas Luar Dalam
    text: Igumedia tidak hanya mementingkan penampilan namun juga mementingkan kualitas
      kode.
  services2:
    heading: Mengkomunikasikan produk
    text: Igumedia berupaya agar website Anda dapat mengkomunikasikan produk dan nilai
      jual Anda dengan baik, sehingga dapat mengkonversi pengunjung menjadi pelanggan.
  services3:
    heading: Berorientasi pada tujuan
    text: Setiap proyek itu unik, website Anda akan dirancang khusus agar memenuhi
      sasaran bisnis Anda. Igumedia akan fokus memasarkan konten Anda melalui desain
      yang jelas, mudah dimengerti dan fungsional.
products:
  header: Website Seperti Apa Yang Nantinya Anda Dapat?
  content:
  - heading: Cepat Dan Stabil
    text: Memanfaatkan teknologi JAM Stack, website Anda akan disajikan dengan cepat
      dan stabil. Jangan khawatir website kamu lama-lama jadi lemot.
  - heading: Website Responsive
    text: Website akan secara otomatis menyesuaikan ukuran dan orientasi layar HP,
      tablet maupun komputer. Tidak perlu zoom dan geser kanan/kiri.
  - heading: Ramah Mesin Pencari (SEO)
    text: Website Anda dibangun dengan mematuhi petunjuk yang telah ditetapkan oleh
      Google. Jadi website Anda akan mudah dicatat oleh mesin pencari, khususnya Google.
  - heading: Mudah Digunakan
    text: Anda akan dengan mudah menambah, mengedit dan menghapus konten pada website
      tanpa perlu bisa pemrograman.
  - heading: Aman Dari Hacker
    text: 'Website Anda akan didistribusikan secara statik sehingga mengurangi kemungkinan
      untuk diretas. '
supports:
  header: Dukungan Aftersales Seperti Apa Yang Kamu Dapatkan ?
  content:
  - heading: Pelatihan Penggunaan Web
    text: Igumedia akan memberikan petunjuk penggunaan website berupa video tutorial
      atau pelatihan secara langsung (khusus Semarang). Bila suatu saat Anda lupa
      atau bingung caranya, Anda tinggal telefon, Igumedia bersedia memandu.
  - heading: Backup Otomatis
    text: Website Anda terhubung dengan private repository Igumedia, backup akan dilakukan
      secara otomatis bila Anda mengaplikasikan perubahan pada website Anda.
  - heading: Dukungan Teknis
    text: Bila suatu saat website Anda rusak, terdapat bug, atau tidak berfungsi secara
      semestinya Igumedia akan bertanggung jawab untuk melakukan perbaikan.
qna:
  header: Kamu punya pertanyaan seputar harga?
  content:
  - question: Apa yang dimaksud Halaman Menu?
    answer: Halaman Menu adalah bagian dari website untuk menampung konten dengan
      tema tertentu. Misalnya halaman Home, halaman About Us, halaman Products, Halaman
      Contact, Halaman Blog, dll.
  - question: Apa yang dimaksud dengan Theme?
    answer: Theme atau Tema adalah keseluruhan tampilan, nuansa, dan gaya situs web.
      Termasuk hal-hal seperti skema warna, tata letak, dan elemen gaya. Intinya,
      Tema situs web kamu adalah representasi langsung dari brand website kamu.
  - question: Apa itu fleksibilitas Theme?
    answer: Dalam membuat theme kami melakukan assestment dan judgement mengenai brand
      kamu. Dari assestmen dan judgment tersebut kami membuat theme yang mempresentasikan
      website kamu. Fleksibilitas theme adalah tingkat keterlibatan pelanggan dalam
      proses pembuatan Theme. Pelanggan dapat menyarankan skema warna, nuansa, tata
      letak, font dll. Pada paket dedicated pelanggan dapat membuat design sepenuhnya
      dan kami akan mengimplementasikannya ke bahasa pemrograman.
  - question: Apa itu prioritas dukungan?
    answer: Bila terdapat beberapa pelanggan yang membutuhkan dukungan (technical
      support) secara bersamaan, maka pelanggan yang memiliki perioritas lebih tinggi
      akan mendapatkan pelayanan lebih dulu dibandingkan pelanggan dengan prioritas
      dukungan lebih rendah.
  - question: Apakah prioritas dukungan mempengaruhi kualitas dukungan?
    answer: Tidak. Kualitas dukungan yang kami berikan pada semua paket sama.
  - question: Apa itu Hosting, Domain dan SSL?
    answer: Hosting adalah ruang yang kamu sewa untuk menyimpan website kami di internet.
      Domain adalah alamat website kamu, misalnya www.facebook.com, www.igumedia.com,
      dll. SSL adalah teknologi untuk mengamankan koneksi antara pengunjung dan server,
      sehingga mencegah orang tidak bertanggung jawab memintas pertukaran data.
  - question: Hosting apa yang akan digunakan?
    answer: Kami memiliki beberapa pilihan hosting, beberapa diantaranya yaitu [Digital
      Ocean](https://www.digitalocean.com), [Amazon AWS](https://aws.amazon.com) dan
      [Netlify](https://www.netlify.com). Penyedia hosting mana yang nantinya digunakan
      tergantung dari kebutuhan website.
  - question: Berapa biaya perpanjangan website?
    answer: Biaya perpanjangan website saat ini adalah Rp 400.000,-/tahun. Biaya perpanjangan
      dapat berubah bila penyedia layanan hosting, domain dan SSL mengubah kebijakan
      yang mempengaruhi harga.
  - question: Berapa lama waktu pembuatan website?
    answer: Lama waktu pembuatan website tergantung dengan skala dan kompleksitas
      website. Secara umum paket Lite membutuhkan waktu 1 - 2 minggu, paket Basic
      2 - 3 minggu, dan paket Dedicated 1 bulan atau lebih.
  - question: Kenapa semakin mahal, waktu pembuatan web semakin lama?
    answer: Semakin mahal paket maka semakin kompleks website yang dihasilkan, semakin
      banyak usaha dan jam kerja yang dikeluarkan. Bila anda mebangun rumah senilai
      2 Miliar tentunya waktu yang diperlukan jauh lebih lama dibandingkan membangun
      rumah senilai 400 juta. Benar, bukan? :)
  - question: Kenapa ada tanda ± pada harga?
    answer: Harga pada pricing table adalah tarif standar paket. Namun bila kamu perlu
      fitur khusus maka ada penyesuaian biaya, atau bila ternyata website yang kamu
      buat tidak terlalu kompleks maka kami tidak keberatan menurunkan tarif.
  - question: Apa sih contoh fitur khusus?
    answer: Fitur khusus tergantung kebutuhan website. Misalnya sebuah perusahaan
      Freight Forwarding berencana membuat website company profile, target pengunjung
      website adalah ekspatriat dan WNI, maka diperlukan fitur multi-bahasa dimana
      website akan disajikan dalam Bahasa Indonesia dan Bahasa Inggris.
  - question: Website yang akan saya buat merupakan website non-profit, apa benar
      ada potongan harga?
    answer: Ya, dengan senang hati. Kami menyediakan potongan harga hingga gratis,
      tergantung dengan keselarasan visi kami. Silahkan siapkan proposal dan hubungi
      kami :)
  - question: Saya bingung paket mana yang cocok untuk saya :|
    answer: Silahkan hubungi kami. Kami akan memberikan saran sesuai kebutuhan Anda.
  - question: Dimana saya bisa mendapatkan informasi yang lebih lengkap?
    answer: Informasi selengkapnya meliputi spesifikasi teknis dan hal-hal lain yang
      perlu anda ketahui dapat Anda baca [disini](/jasa-pembuatan-website/pendahuluan-layanan-pembuatan-website/).
  - question: Saya memiliki pertanyaan yang belum terjawab
    answer: Silahkan hubungi kami melalui WA 0813-2577-9521, email hello@igumedia.com
      atau melalui chat di pojok kanan bawah.
order:
  header: Bagaimana proses pembuatan website?
  description: ''
  content:
  - header: Komunikasi
    text: Komunikasi adalah kunci dalam membangun suatu website. Mendefinisikan output
      yang akan dicapai.
  - header: Perencanaan
    text: Pembuatan rancangan fitur, desain sistem dan task schedule. Penerbitan proforma
      invoice dan down payment.
  - header: Pembuatan Web
    text: Proses pembuatan website berdasarkan task schedule, melakukan penyesuaian
      dan launching website.
  - header: Pelatihan & Perawatan
    text: Memberikan pelatihan kepada Klien mengenai cara penggunaan website dan monitoring
      kinerja website secara continous.
final:
  header: Semuanya dimulai dengan langkah pertama
  description: Website seperti apa yang ingin kamu buat ?
  cta: Hubungi Saya Sekarang
pricing:
  header: Berapa Harga Untuk Mendapatkan Website Impianmu?
  description: Pada dasarnya tarif pembuatan website tergantung pada fitur dan work-hours
    yang dibutuhkun. Namun berikut Igumedia telah memetakan tarif standar pembuatan
    website dari skala kecil hingga besar bila Anda tidak memerlukan fitur-fitur khusus.
  note: "*Bila tidak terdapat paket yang sesuai dengan kebutuhan Anda atau Anda butuh
    fitur khusus silahkan hubungi Customer Support"
  price1:
    name: DEDICATED
    price: 4,5 Jt
    recommended: true
    text: Cocok untuk institusi, sekolah, perusahaan, portal berita, blog besar maupun
      e-commerce
    list:
    - Dedicated Development Process
    - Jumlah halaman sesuai kebutuhan
    - Fleksibilitas theme tinggi
    - Dukungan prioritas tinggi
    - Hosting, Domain, SSL
  price2:
    name: STANDARD
    price: 2,5 Jt
    recommended: false
    text: Cocok untuk company profile, brosur online dan website informasional
    list:
    - Maks 6 Halaman Menu
    - Flexibilitas theme menengah
    - Dukungan prioritas menengah
    - Domain, Hosting, SSL
  price3:
    name: Lite
    price: 1,5 Jt
    recommended: false
    text: Cocok untuk personal, website event (konser,dll), dan brosur online.
    list:
    - Maks 3 Halaman menu
    - Fleksibikitas theme rendah
    - Dukungan prioritas rendah
    - Hosting, Domain, SSL
  price4:
    name: Price name
    price: 4,5 Jt
    recommended: false
    text: Cocok untuk pemula dan anak-anak
    list:
    - list 1
    - list 2
    - list 3
    - list 4
    - list 5

---
