---
title: Responsive web design - Apa itu, Manfaat dan Kekurangannya
description : Responsive web design adalah pendekatan untuk mobile-friendly website, dengan responsive design tidak perlu membaut dan maintenance 2 website secara bersamaan, namun responsive website tidak terlalu optimal untuk telepon genggam.
authors: Irfan Gumelar
categories: Web Design
date: 2019-02-08 09:44:23 +0000
related: Web Design

---
#### Latar belakang

Saat ini website tidak hanya diakses melalui komputer namun juga telepon genggam. Namun perbedaan dimensi dan kebiasaan antara layar komputer dan telepon genggam menimbulkan masalah aksesibilitas pada website.

Website yang dibuat khusus pada layar komputer kerap kali terlalu luas dan tidak kompatibel dengan telepon genggam. Pengguna telepom genggam harus men-_zoom_ dan menggeser kanan-kiri untuk mengkonsumsi seluruh tampilan website.

Selain itu terdapat fitur yang bekerja pada komputer namun tidak bekerja pada telepon genggam sehingga pengguna telepon genggam tidak dapat mengkonsumsi secara utuh informasi yang ada pada website.

Masalah tersebut yang melatarbelakangi kebutuhan untuk website dapat diakses secara ramah baik dikomputer maupun telepon genggam. Komponen penting pada website harus dapat disajikan secara utuh dan mudah dicerna baik dikomputer maupun telepon genggam. Konsep tersebutlah yang disebut **mobile-friendly** website.

#### Metode pembuatan mobile-friendly website

Ada tiga metode utama untuk mengimplementasikan situs web yang mobile-friendly. Berikut adalah perbedaan utama dari setiap metode menggunakan bahasa yang sesederhana mungkin:

* **Responsive web design :** Versi website komputer dan telepon genggam menggunakan <mark>alamat website yang sama</mark> dan memiliki <mark>basis kode yang sama</mark>, namun ditampilkan secara berbeda sesuai ukuran layar _device_ yang digunakan pengunjung.
* **Dynamic serving :** Versi website komputer dan telepon genggam menggunakan <mark>alamat website yang sama</mark> namun memiliki <mark>basis kode yang berbeda</mark>. Kode yang ditampilkan sesuai dengan jenis _device_ yang digunakan pengunjung.
* **Situs mobile terpisah :** Versi website komputer dan telepon genggam menggunakan <mark>alamat website yang berbeda</mark> dan memiliki <mark>basis kode yang berbeda pula</mark>.

> Dari penjelasan diatas kita ketahui bahwa **responsive design adalah metode implementasi** untuk mendapatkan situs web yang mobile friendly.

Mungkin saya akan membedah masing-masing metode implementasi pada post mendatang. Namun pada post ini saya akan fokus pada responsive web design.

#### Apa itu responsive web design?

**Responsive web design** adalah metode implementasi dimana suatu website mempunyai kode yang sama dan URL yang sama untuk versi komputer dan telepon genggam, melalui kode tersebut tampilan website akan menyesuaikan sesuai dengan orientasi dan ukuran layar _device_ yang digunakan oleh pengunjung.

Tujuan dari desain responsive web design adalah untuk menghindari _resizing_, _horizontal scrolling_, atau _zooming_ yang tidak perlu dalam mengakses situs web. Responsive web design mengeliminasi kebutuhan untuk memiliki dua versi situs yang berbeda untuk satu situs web.

#### Apa manfaat responsive web design

* **Lebih mudah dan lebih murah untuk perawatan**  
  Menggunakan responsive web design kita mengelimanasi kebutuhan untuk pembuatan dan perawatan 2 versi website yang berbeda. Satu basis kode dapat digunakan untuk seluruh perangkat. Dengan demikian tidak perlu membuat perubahan pada 2 website.
* **Memudahkan pengguna untuk berbagi link dengan satu URL yang sama**
* **Metode yang direkomendasikan oleh Google**
* **Proses engineering yang lebih mudah**
  Secara teknik pembuatan responsive web design lebih mudah dibanding dua metode lainnya. Responsive design tidak perlu URL management, redirection, annotation dan device detection.

#### Apa kekurangan responsive web design?
* **Website besar akan baik-baik saja dimuat di komputer namun mungkin akan lambat dimuat di telepon genggam.**  
  Website besar biasanya membutuhkan sumber daya yang tinggi yang tidak masalah bila dimuat di komputer atau telepon genggam terbaru. Namun telepon genggam tua akan kesulitas memuat situs yang tidak "dirampingkan" khusus untuk telepon genggam.
* **Tidak dibangun dan dioptimisasi khusus untuk telepon genggam.**

#### Kesimpulan dan penutup

Responsive web design menyediakan jalan untuk mendapatkan sistus web mobile-friendly dengan hemat biaya dan mudah dalam pengembangan dan perawatan. Tapi karena satu kode didesain untuk seluruh perangkat maka optimisasi untuk telepon genggam tidak optimal.

Metode manakah yang paling baik untuk digunakan?  Metode yang terbaik adalah yang paling sesuai dengan situasi Anda dan yang dapat memberikan _User Experience_ terbaik.

Apa pun metode implementasi yang digunakan, ada satu hal yang jelas:  situs web Anda harus mobile-friendly. Situs yang mobile-friendly mempunyai _user experience_ yang baik dan akan meningkatkan peringkat Anda pada mesin pencari.