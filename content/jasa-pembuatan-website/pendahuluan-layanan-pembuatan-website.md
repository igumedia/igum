+++
date = "2019-02-05 16:23:11 +0000"
description = "Penjelasan seluruh spesifikasi layanan jasa pembuatan website. Akan membahas mengenai igumedia, fitur yang fundamental pada website, fitur khusus, proses pembuatan, teknologi yang digunakan serta syarat dan ketentuan"
title = "Pendahuluan layanan pembuatan website"
posttitle = "Pendahuluan"
[menu.infoweb]
name = "Pendahuluan"
weight = 1

+++
Kewajiban penjual jasa kepada calon pelanggan adalah menjelaskan seluruh spesifikasi jasa yang akan dijual sesuai dengan hakikatnya tanpa ada aib yang disembunyikan.  Calon pelanggan memiliki hak memilih untuk memilih meneruskan transaksi atau membatalkan transaksi.

Saya akan menjelaskan secara terperinci seluruh informasi yang perlu anda ketahui mengenai jasa yang kami tawarkan. Banyak dari informasi teknis akan saya sampaikan dengan penyampaian yang sederhana agar dapat mudah dimengerti oleh pembaca non-teknis.

Berikut adalah daftar isi mengenai apa saja yang akan kami bahas :

* [Tentang Igumedia](/jasa-pembuatan-website/tentang-igumedia "Tentang Igumedia")
* [Fitur penting yang wajib ada pada setiap website](/jasa-pembuatan-website/fitur-penting-yang-wajib-ada-pada-setiap-website/ "Fitur penting yang wajib ada pada setiap website")
* [Fitur khusus yang diperlukan oleh website tertentu](/jasa-pembuatan-website/fitur-spesifik-yang-diperlukan-oleh-website-tertentu/ "Fitur khusus yang dibutuhkan website tertentu")
* [Teknologi yang Digunakan](/jasa-pembuatan-website/teknologi-yang-digunakan-dalam-pembuatan-website/ "Teknologi inti yang digunakan dalam membangun website")
* [Proses non-teknik pembuatan website](/jasa-pembuatan-website/proses-non-teknis-pembuatan-website/ "Proses non-teknik pembuatan website")
* [Syarat dan Ketentuan](/jasa-pembuatan-website/syarat-dan-ketentuan/ "Syarat dan ketentuan jasa pembuatan website")

> **Note :** Bagian "info selengkapnya" ini masih dalam pengembangan dan akan masih terus disempurnakan. Bila ada pertnyaan yang masih belum terjawab dalam benak Anda silahkan tanya langsung melalui contact person yang tersedia.

Untuk memulai seluruh penjelasan ini, silahkan klik **Artikel Berikutnya**.