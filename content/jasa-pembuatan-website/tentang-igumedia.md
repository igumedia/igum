+++
date = "2019-02-18T07:12:38+00:00"
description = "Igumedia adalah one-man web development business, Igumedia fokus pada web informasional seperti brosu online, company profile, blog, website sekolah, dll"
title = "Tentang Igumedia"
posttitle = "Tentang Igumedia"
[menu.infoweb]
weight = 2

+++
Igumedia adalah _one-man web development business_ yang digagas oleh Irfan Gumelar. Igumedia berspesialisasi dalam membangun website informasional seperti brosur online, company profile, blog, portal majalah/berita, website institusi dan sekolah.

Pendekatan saya dalam membangun sebuah website yaitu dengan menempatkan customer Anda dan SEO sebagai prioritas utama. Saya memandang proyek pembuatan website dari perspektif customer anda  untuk membantu Anda menyajikan informasi yang tepat untuk membangun bisnis Anda.

> **_Tempat penampilan, Tepat penyampaian, Tepat penyajian_**

Saya berdomisili di Semarang, Indonesia dan saya sangat tertarik untuk mengerjakan proyek web design, web development, atau front-end development untuk Klien baik di Semarang maupun luar Semarang.

Sedikit background mengenai saya :

* Sarjana Komputerisasi Akuntansi, Politeknik Negeri Semarang
* Ex-Programmer pada suatu _Software House_ di Pekalongan
* Telah mengikuti course _Web Design for Web Developers_
* Telah mengikuti course _Advanced CSS and Sass: Flexbox, Grid, Animations_
* Telah mengikuti course _The Complete JavaScript Course 2019: Build Real Projects!_