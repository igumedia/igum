+++
date = "2019-02-24T07:18:52+00:00"
description = "Anda dapak merequest fitur khusus untuk website Anda. Fitur khusus yaitu fitur yang dibutuhkan secara khusus oleh suatu website untuk mengakomodasi kebutuhan khusus."
title = "Fitur spesifik yang diperlukan oleh website tertentu"
posttitle = "Fitur spesifik"
[menu.infoweb]
name = "Fitur khusus untuk website tertentu"
weight = 4

+++
Fitur spesifik berbeda dengan fitur umum yang sebelumnya telah dijelaskan, tidak semua website membutuhkan fitur spesifik. Fitur spesifik yaitu fitur yang dibutuhkan secara khusus oleh suatu website untuk mengakomodasi kebutuhan khusus. Fitur spesifik berbeda antara satu website dengan website lain tergantung dengan kebutuhan masing-masing.

Fitur spesifik dapat di-request secara khusus oleh Anda atau atas saran dari kami, dalam hal ini tentunya tidak wajib. Penerapan fitur spesifik dikenakan tarif tambahan sesuai tingkat kesulitan fitur yang diimplementasikan.

Sebagai contoh implementasi fitur spesifik, misalnya sebuah perusahaan Freight Forwarding berencana membuat website company profile dan untuk menawarkan jasanya, target pengunjung website adalah ekspatriat dan WNI, maka diperlukan fitur multi-bahasa dimana website akan disajikan dalam Bahasa Indonesia dan Bahasa Inggris.

Contoh lain, suatu institusi penyandang disabilitas membutuhkan website yang informasional. Beberapa target pengunjung adalah penyandang disabilitas visual, maka dibutuhkan website yang ramah untuk penyandang disabilitas visual. Oleh karena itu website harus dibangung dengan pendekatan [Web Accessibility In Mind (Web AIM)](https://webaim.org/ "Web AIM (Web Accessibility In Mind)")  dan mematuhi [Web Content Accessibility Guidelines (WCAG)](https://www.w3.org/WAI/standards-guidelines/wcag/ "Web Content Accessibility Guidelines") untuk memastikan website dapat diakses oleh siapa saja.