+++
date = "2019-02-26T00:23:40+00:00"
description = "Proses pembuatan wesite yaitu komunikasi, untuk mendefiniskan output. Perencanaan, yaitu langkah untuk menggapai output, Coding, lalu Pelatihan dan Perawatan."
title = "Proses non-teknis pembuatan website"
posttitle = "Proses Pembuatan"
[menu.infoweb]
name = "Proses non-teknis pembuatan website"
weight = 6

+++
### Komunikasi

Tahap awal pengembangan website adalah komunikasi. Selama fase ini kita akan mendiskusikan dan mengidentifikasi sasaran bisnis yang Anda targetkan dan mengembangkan strategi yang akan digunakan untuk mencapai tujuan tersebut. Hasil kerja fase ini seringkali berupa spesifikasi teknis, peta situs, dan strategi penyajian informasi.

### Perencanaan

Setelah target output terdefinisikan, saya melakukan riset tentang website seputar industri Anda baik dalam dan luar negeri. Saya mengumpulkan informasi mengenai _what's work and what doesn't._

Berdasar informasi hasil riset saya membuat kerangka website, base layout dan desain sistem navigasi yang dapat mendorong pengujung mengeksplore website Anda lebih dalam.

Selanjutnya kerangka website di-_breakdown_ menjadi modul-modul fitur.   Langkath terakhir dalam perencanaan yaitu pembutan _task schedule_ modul-modul fitur. Task Schedule tersebut yang menjadi target pencapaian pembangunan website setiap harinya.

### Pembuatan Web

Tahap ini oleh para programmer disebut _ngoding_. Dalam bahasa yang sederhana saya melakukan implementasi perencanaan yang telah dibuat hingga website selesai.

Pada tahap ini juga dilakukan pengujian dan penyesuaian-penyesuaian hingga website memenuhi output yang ditetapkan. Ujung dari tahap ini adalah _launching_ website.

### Pelatihan dan Perawatan

Setelah website dapat diakses oleh publik maka saya akan memberikan pelatihan penggunaan website. Pelatihan dapat dilakukan menggunakan video tutorial atau pelatihan secara langsung (khusus Semarang).

Saya juga akan memantau secara berkala kinerja website Anda. Bila terdapat _bug_ atau hal yang tidak semestinya maka saya akan memperbaikinya.