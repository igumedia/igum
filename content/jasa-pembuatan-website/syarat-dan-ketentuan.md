+++
date = "2019-02-26T09:56:51+00:00"
description = "Syarat dan ketentuan pembuatan website yang harus dipatuhi demi ketertiban transaksi"
title = "Syarat dan Ketentuan"
posttitle = "Syarat dan Ketentuan"
[menu.infoweb]
weight = 7

+++
#### Umum

* Dengan memesan Website pada Igumedia berarti anda telah menyetujui syarat dan ketentuan yang berlaku yang terdapat pada laman ini.
* Pemberitahuan tertulis dapat dilakukan melalui e-mail, chat, atau sms.

#### Igumedia

* Igumedia bertanggung jawab untuk membuat website.
* Igumedia akan melakukan upaya yang wajar untuk memberikan layanan sesuai dengan jadwal yang ditetapkan dalam perencanaan; Namun, Igumedia tidak memberikan jaminan layanan akan terpenuhi sesuai jadwal.

#### Pelanggan

* Pelanggan bersedia bekerjasama seperti yang disyaratkan oleh Igumedia untuk memungkinkan Igumedia melaksanakan kewajibannya
* Pelanggan bersedia menyediakan semua informasi dan dokumen yang diperlukan oleh Igumedia sehubungan dengan penyediaan layanan.
* Pelanggan bersedia untuk mengadakan kerjasama dengan pihak ketiga yang secara wajar diharuskan oleh Igumedia untuk memungkinkan Igumedia memenuhi kewajibannya.

#### Penyerahan website

* Igumedia akan membuat pemberitahuan secara tertulis kepada pelanggan ketika website diserahkan.
* Periode penerimaan yaitu tiga kali dua puluh empat jam sejak Pelanggan menerima pemberitahuan penyerahan website.
* Selama periode penerimaan Pelanggan bertanggungjawab melakukan tes penerimaan untuk menentukan :
  1. apakah website secara material sesuai dengan spesifikasi yang diajukan;
  2. apakah website memiliki cacat.

#### Penerimaan website

* Jika menurut pendapat wajar Pelanggan, website memenuhi Kriteria Penerimaan, Pelanggan akan mengirimkan pemberitahuan tertulis kepada Igumedia selama periode penerimaan yang mengkonfirmasikan website telah diterima oleh Pelanggan.
* Jika menurut pendapat wajar Pelanggan, website tidak memenuhi Kriteria Penerimaan, Pelanggan akan mengirimkan pemberitahuan tertulis kepada Igumedia selama periode penerimaan yang menguraikan secara terperinci tentang hal-hal dimana website tidak memenuhi Kriteria Penerimaan.
* Jika Igumedia setuju bahwa website tidak memenuhi Kriteria Penerimaan, Igumedia akan memiliki periode perbaikan selama 30 hari kerja untuk memodifikasi website sehingga memenuhi Kriteria Penerimaan.
* Website dianggap telah diterima oleh pelanggan jika :
  1. Pelanggan tidak memberikan pemberitahuan kepada Igumedia selama Periode Penerimaan;
  2. Website telah dimodifikasi oleh pihak selain Igumedia, Pelanggan, atau Pihak ketiga yang diakui oleh Igumedia;

#### Biaya dan Pembayaran

* Website yang dipesan tidak akan mulai dikerjakan hingga pelanggan melaksanakan tanggungjawab pembayaran.
* Termin pembayaran yaitu 50% - 50%, 50% pembayaran dimuka ketika diterbitkan proforma invoice dan 50% pelunasan paling lambat 7 hari sejak periode penerimaan berakhir.
* Pembayaran dilakukan melalui transfer antar bank.
* Bila ditengah proses pembuatan website pelanggan mengajukan penambahan/pengurangan/perubahan fitur yang bersifat major maka akan dilakukan kalkulasi ulang biaya pembuatan website.

#### Pembatalan proyek secara sepihak

* Bila Igumedia membatalkan proyek secara sepihak maka Igumedia akan mengembalikan uang muka seluruhnya kepada Pelanggan.
* Bila Pelanggan membatalkan proyek secara sepihak Pelanggan akan membayar sejumlah kerugian yang diderita oleh Igumedia. Bila jumlah kerugian yang diderita kurang dari uang muka maka Igumedia akan mengembalikan kelebihan uang muka sedangkan bila sebaliknya Pelanggan akan membayar sisa kerugian.

#### Dukungan

* Dukungan akan diberikan kepada Pelanggan selama website masih dalam naungan Igumedia.
* Dukungan akan diberikan sesuai dengan prioritas dukungan.
* Waktu tunggu website Paket Dedicated paling lama 3 hari kerja.
* Waktu tunggu website Paket Standard paling lama 5 hari kerja.
* Waktu tunggu website Paket Lite paling lama 7 hari kerja.

#### Pelatihan

* Pelatihan penggunaan website akan dilakukan melalui video tutorial yang akan diberikan secara digital.
* Pelatihan secara langsung hanya dilakukan di Semarang pada waktu yang disanggupi oleh Igumedia.
* Pelatihan secara langsung gratis hanya untuk pertemuan pertama.
* Bantuan penggunaan website dapat ditanyakan melalui WhatsApp, Live Chat, Slack, telefon dan email.

#### Backup

* Backup dilakukan secara otomatis namun Igumedia tidak menyediakan garansi dalam bentuk apapun bila terjadi kehilangan data.

#### Pengawasan dan Perawatan

* Pelanggan memberikan izin kepada Igumedia untuk mengakses hosting dan domain.
* Pelanggan memberikan izin kepada Igumedia untuk melakukan perubahan-perubahan yang diperlukan pada website, hosting dan domain.

#### Pihak Ketiga

* Karya Pihak Ketiga akan dilisensikan kepada Pelanggan berdasarkan syarat dan ketentuan standar pemberi lisensi, atau sesuai ketentuan lisensi yang diberitahukan oleh Igumedia kepada Pelanggan.
* Segala biaya untuk pembayaran Pihak Ketiga akan dibayarkan oleh Pelanggan sebagai tambahan dari tarif pembuatan website (kecuali para pihak menyetujui sebaliknya).

#### Kepatuhan Hukum

* Pelanggan akan memastikan bahwa karya Pelanggan tidak melanggar hukum, peraturan, atau hak pihak ketiga.
* Igumedia tidak bertanggung jawab dan akan dibebaskan atas segala tuntutan sebagai akibat klaim bahwa Karya Pelanggan merupakan Konten yang Melanggar Hukum.

#### Batasan kewajiban

* Igumedia tidak akan bertanggung jawab atas :
  1. kehilangan laba, pendapatan, atau tabungan yang diantisipasi;
  2. kehilangan atau kerusakan data, database, atau perangkat lunak apa pun;
  3. kerugian reputasi atau Goodwill;
  4. hilangnya peluang komersial;
  5. kerugian tidak langsung, kerugian khusus, dan kerugian konsekuensial;
  6. perselisihan antara pelanggan dengan konsumennya dengan alasan apapun.
* Igumedia tidak akan bertanggung jawab atas kerugian yang timbul karena peristiwa Force Majeur.

#### Penghentian Layanan

* Igumedia dapat mengakhiri layanan kapan saja dengan memberikan pemberitahuan tertulis setidaknya 30 hari kepada Pelanggan.
* Pelanggan dapat mengakhiri langganan layanan kapan saja dengan memberikan pemberitahuan tertulis setidaknya 30 hari kepada Igumedia.

#### Hosting dan Domain

* Seluruh biaya yang dibutuhkan untuk pengadaan dan perpanjangan hosting dan domain menjadi tanggung jawab Pelanggan.
* Pelanggan tidak akan mendapatkan akses ke dashboard hosting dan domain.
* Harap dicatat bahwa Igumedia menyediakan layanan hosting web melalui kemitraan dengan Netlify Free Tier. Layanan ini menyediakan layanan hosting yang menampung beberapa situs web dalam satu akun penanggung jawab. Pendekatan ini menyediakan solusi hosting web yang hemat biaya, tetapi mungkin tidak sesuai untuk semua situs web. Jika Anda memiliki kekhawatiran tentang apakah hosting pilihan Igumedia sesuai untuk situasi Anda, silakan hubungi Igumedia di 0813-2577-9521 atau dengan mengirim email ke hello@igumedia.com.
* Perpanjangan masa berlaku hosting dilakukan selambat-lambatnya 30 hari sebelum masa berlaku hosting dan domain berakhir.