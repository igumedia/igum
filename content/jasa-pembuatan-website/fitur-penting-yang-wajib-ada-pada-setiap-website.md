+++
date = "2019-02-20T08:40:22+00:00"
description = "Igumedia menyediakan jasa pembuatan website dengan fitur yang wajib dimiliki seperti responsive website, ramah SEO, Keamanan dan Content Management System."
title = "Fitur penting yang wajib ada pada setiap website"
posttitle = "Fitur Fundamental"
[menu.infoweb]
name = "Fitur yang wajib ada pada setiap web"
weight = 3

+++
Igumedia menyediakan fitur fundamental yang tersedia secara gratis untuk seluruh paket harga. Fitur-fitur ini secara umum merupakan fitur yang wajib ada pada setiap website modern. Keberadaan fitur tersebut ditujukan untuk meningkatkan _user experience,_ SEO, dan keamanan.

> **_User Experience_** adalah pengalaman keseluruhan seseorang menggunakan produk seperti website atau _software_, terutama dalam hal seberapa mudah atau menyenangkan untuk digunakan

Berikut adalah pemaparan fitur-fitur tersebut.

### Responsive Website

**Responsive website** adalah perilaku website yang tanggap terhadap orientasi dan ukuran layar. Website yang responsif akan secara otomatis menyesuaikan layout dan konten sesuai dengan ukuran layar yang digunakan oleh pengguna.

> Responsive website kadang juga dikenal sebagai mobile-friendly atau tablet-friendly website.

Responsive website memastikan  website ramah digunakan dan mudah diakses tanpa memaksa pengguna untuk melakukan _zooming, slide_ ke samping kanan/kiri atau memicingkan mata.

Berikut adalah contoh website non-responsive vs website responsive :

![Website non-responsive vs Website responsive](/uploads/responsive vs non responsive.jpg "Website non-responsive vs Website responsive")

Alasan utama untuk menggunakan _responsive website_ adalah untuk meningkatkan jangkauan website Anda ke basis pengguna yang lebih besar dengan perangkat yang beragam.

#### Informasi Teknis

Dalam mengembangkan responsive website Igumedia menyesuaikan layout, font, dan _art-direction_ pada setiap _breakpoint_. Igumedia menghasilkan responsive website dengan menyimulasikan website pada _development tools_ dan melakukan testing pada beberapa smartphone secara langsung.

> **Breakpoint** didefinisikan sebagai suatu titik-pembatas, bila dipenuhi, memicu perubahan dalam desain.

### Search Engine Optimization (SEO)

Agar konsep mengenai SEO lebih mudah dimengerti mari kita lakukan simulasi. Kita posisikan Anda sebagai calon konsumen.

> Suatu saat Anda ingin mencari supplier madu asli untuk bisnis Anda. Anda lalu coba mencarinya melalui Google.com dengan kata kunci **Supplier Madu Asli**. Pada hasil pencarian akan ditampilkan 10 hasil pencarian organik (non-iklan) dan beberapa iklan (bila ada).
>
> Selanjutnya anda pasti akan memilah satu persatu dari hasil pencarian yang ada, bila menemukan hasil yang sesuai Anda dapat mulai bertransaksi, sedangkan bila Anda tidak menemukan hasil seperti yang anda harapkan apa yang akan Anda lakukan? Ada 2 kemungkinan, anda akan mencari pada halaman ke-2 atau mencari ulang dengan kata kunci lain.

Dari simulasi diatas dapat kita ambil pelajaran seberapa penting untuk tampil pada **halaman pertama** mesin pencarian. Bila situs tampil pada halaman pertama akan meningkatkan kemungkinan situs Anda dikunjungi oleh calon pelanggan. Sedangkan bila situs Anda ditampilkan pada halaman ke 2, 3, 4, dst maka kemungkinan situs Anda dikunjungi calon pelanggan akan semakin kecil karena bisa saja calon pelanggan justru melakukan pencarian ulang dengan kata kunci yang berbeda yang menyebabkan situs Anda terabaikan.

Segala usaha untuk menampilkan situs/website pada halaman pertama hasil pencarian itulah yang kerap dikenal dengan **Search Engine Optimization (SEO)**

> **Search Engine Optimization (SEO)** atau dalam Bahasa Indonesia Optimisasi Mesin Pencari adalah proses meningkatkan visibilitas online dari suatu situs web dalam hasil mesin pencarian tanpa memanfaatkan fitur berbayar (iklan).

Website dengan SEO yang baik akan memiliki kemungkinan yang lebih besar tampil pada halaman pertama mesin pencarian, hal tersebut dapat meningkatkan lalu lintas pengunjung pada website Anda.

#### Informasi Teknis

SEO merupakan kombinasi optimisasi pada bidang teknis (meta-tag, robot.txt, sitemap, dll) dan bidang non-teknis (penulisan, copy-writing, content marketing, komunitas, dll). Dalam membangun website Igumedia fokus melakukan optimisasi teknis.

Hasil dari optimisasi teknis ini dapat diukur dengan audit Google Lighthouse, Igumedia berusaha sebaik mungkin agar mendapatkan nilai sempurna (100) pada metrik SEO Google Lighthouse .

Optimisasi teknis saja tentunya tidak cukup untuk menghantarkan situs anda untuk memuncaki hasil pencarian. Anda harus menyediakan konten yang berkualitas, menyajikan informasi yang diinginkan pengujung dengan komprenhensif,  menjaga informasi selalu _up-to-date_ dan terpecaya_._

> Dalam dunia SEO, **konten adalah raja.**

Dengan melakukan optimisasi teknis maka saya telah memberikan Anda pedang untuk menaklukan naga. Namun bagaimana Anda memanfaatkan pedang tersebut kembali kepada Anda. Tapi jangan khawatir, banyak _resource_ diluar sana sebagai sumber belajar dan saya akan berbagi informasi serta tips mengenai SEO & Content Marketing pada blog saya secara berkala.

### Content Magangement System (CMS)

**Content Management System (CMS)** adalah suatu aplikasi yang digunakan untuk mengelola konten digital. CMS memberikan kemudahan kepada pengguna untuk mengelola konten website tanpa menyentuh bahasa pemrograman secara langsung. Melalui CMS Anda dapat melakukan penambahan, pengurangan dan penambahan konten dari komputer maupun HP.

#### Informasi Teknis

CMS yang Igumedia gunakan adalah Forestry CMS. Beberapa fitur yang didukung oleh Forestry yaitu WYSIWYG Editor, Media Library dan kustomisasi sesuai tema yang digunakan.

### Keamanan Website

Beberapa kali dapat dilihat melalui media pemberitaan website-website milik pemerintah menjadi korban para peretas. Para peretas mampu merubah tampilan website dan menyisipkan pesan-pesan mereka sendiri.

Hal tersebut bila terjadi kepada Anda maka dapat melukai reputasi Anda. Selain terlihat tidak profesional hal tersebut dapat membuat pengunjung ragu terhadap Anda, bila Anda tidak mampu menjaga keamanan website bagaimana pengunjung dapat mempercayakan data pribadinya? apalagi bila menyangkut transaksi keuangan.

Lebih buruk lagi bila para peretas bekerja dalam senyap, dibalik layar mencuri informasi tanpa diketahui. Aksi seperti itu tentunya tidak hanya merugikan   Anda namun juga pelanggan Anda. Itulah  kenapa keamanan menjadi salah satu komponen penting pada website, keamanan tidak hanya melindungi Anda sebagai pemilik website namun juga pelanggan dan pengunjung website Anda.

#### Informasi Teknis

Salah satu fungsi keamanan ialah menjaga integritas data. Website dengan kemanan yang baik akan menjaga saluran informasi antara pengunjung dengan server tidak dapat dibaca maupun dimanipulasi oleh pihak ketiga. Hal tersebut kami wujudkan dengan mengimplementasikan **SSL Let's Encrypt** pada website Anda.

Salah satu jalan yang digunakan peretas untuk melancarkan aksinya yaitu dengan mengeksploitasi celah-celah yang ada pada _database_ dan ekosistemnya.   Karena Igumedia menggunakan arsitektur [JAM Stack](https://jamstack.org/ "JAM Stack"), kami mengeliminasi database dan _backend programming_ pada website sehingga mengurangi celah yang dapat dieksploitasi oleh peretas.

> Strategi keamanan lainnya saya rahasiakan demi menjaga keamanan

### Kesimpulan

Dalam menyediakan jasa pembuatan website Igumedia beberapa fitur umum sebagai bagian fundamental dari website modern. Fitur tersebut yaitu responsive website, SEO, CMS dan Keamanan.

> * **Responsive website** dibangun dengan menyimulasikan website dan melakukan adjusment pada setiap breakpoint serta testing pada beberapa device secara lansung.
> * **SEO** dilakukan dengan melakukan optimisasi teknis seperti robot.txt, sitemap, meta-tags, dll
> * **CMS** menggunakan Forestry untuk mengelola website
> * Penerapan **keamanan** dilakukan dengan menimplementasikan Let's Encrypt SSL dan menggunakan arsitektur JAM Stack.