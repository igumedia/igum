+++
date = "2019-02-25T01:54:55+00:00"
description = "Jasa pemnbuatan website menggunakan JAM Stack sebagai dasar arsitektur. Memanfaatkan keunggulan situs statik untuk mengurangi overhead pada data-driven CMS"
title = "Teknologi yang digunakan dalam pembuatan website"
posttitle = "Teknologi Inti"
[menu.infoweb]
name = "Teknologi yang digunakan"
weight = 5

+++
> Pada bagian ini akan dijelaskan mengenai teknologi inti yang digunakan dalam membangun website yang mungkin akan sedikit membingungkan bagi pembaca awam. Bila Anda tidak tertarik untuk mengetahui teknologi dan arsitektur yang digunakan maka anda dapat melewati bagian ini.

Igumedia memilih untuk mengembangkan website secara statik untuk memaksimalkan performa dan keamanan, selain itu juga mengeliminasi _overhead_ yang ada pada _Database Based CMS_ seperti Wordpress. Untuk memudahkan _maintenance_ dan _development_ Igumedia menggunakan Hugo sebagai Static Site Generator (SSG).

#### Arsitektur Teknologi

Hosting yang digunakan oleh Igumedia adalah hosting yang telah dioptimalisasi untuk situs statik. Oleh karena itu hosting tidak memanfaatkan _backend-language_ dan _database._ Dengan strategi tersebut maka website diharap akan lebih tanggap terhadap _request_ dari pengunjung dan _harder-to-break._

Bila dibutuhkan fitur yang memerlukan akses database maka harus ditambahkan hosting diluar hosting utama atau menggunakan service pihak ketiga seperti _SaaS_, _DaaS_, atau _PaaS_.

Penerapan arsitektur seperti diatas dapat dianalogikan seperti memisahkan telur pada beberapa keranjang. Website utama anda disimpan pada hosting utama, sedangkat fitur anda yang membutuhkan database disimpan pada hosting lain. Bila fitur lain tersebut berhasil diretas atau mengalami _down_ maka tidak akan mempengaruhi website utama. Sedangkan Website utama Anda akan lebih kuat terhadap serangan peretas karena bersifat statik.

> Jangan menaruh seluruh telur dalam satu keranjang